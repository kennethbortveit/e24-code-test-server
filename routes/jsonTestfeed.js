module.exports = function(app, fs, request){
	
	app.get('/jsonTestfeed', function(req, res){
		var url = 'http://static.e24.no/testfeed.json';	
		request({
			url: url,
			json: true
		}, function(error, response, body){
			if(!error && response.statusCode === 200){
				res.send(body);
			}
			else {
				console.log('Error: Could not get json data.');
			}
		});
	});
	
};
