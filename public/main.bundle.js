webpackJsonp([1,4],{

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__json_service__ = __webpack_require__(60);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JsonComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var JsonComponent = (function () {
    function JsonComponent(jsonService) {
        this.jsonService = jsonService;
    }
    JsonComponent.prototype.ngOnInit = function () {
        this.getArticles();
    };
    JsonComponent.prototype.getArticles = function () {
        var _this = this;
        this.jsonService.getJsonTestData().subscribe(function (articles) {
            _this.articles = articles;
        });
    };
    return JsonComponent;
}());
JsonComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'json',
        styles: ["\n\t\th2 {\n\t\t\tcolor: #ffffff;\n\t\t}\n\n\t\t.text-bold {\n\t\t\tfont-weight: bold;\n\t\t}\n\n\t\t.link-background\u00A0{\n\t\t\tbackground-color: #dd0000 !important;\n\t\t}\n\n\t"],
        template: "\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-12\">\n\t\t\t\t<h2 class=\"my-5 text-center\">JSON</h2>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-12\">\n\t\t\t\t<div class=\"card my-3\" *ngFor=\"let article of articles\">\n\t\t\t\t\t<div class=\"card-block\">\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-12 col-lg-4\">\n\t\t\t\t\t\t\t\t<p class=\"card-text text-lg-left text-bold\">{{article.getTime()}}</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-12 col-lg-4\">\n\t\t\t\t\t\t\t\t<p class=\"card-text text-lg-center text-bold\">{{article.getDate()}}</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t\t<div class=\"col-12 col-lg-4\">\n\t\t\t\t\t\t\t\t<p class=\"card-text text-lg-right text-bold\">{{article.getCategory()}}</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t\t<h4 class=\"card-title mt-3\">{{article.getTitle()}}</h4>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t\t<p class=\"card-text\">{{article.getDescription()}}</p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t\t<!-- Incorrect use of CSS -->\n\t\t\t\t\t\t\t\t<p class=\"card-text text-right text-bold mt-3\"><a href=\"{{article.getLink()}}\" target=\"_blank\"><button class=\"btn btn-primary\" style=\"background-color: #dd0000; border: #dd0000; cursor: pointer;\">G\u00E5 til artikkel</button></a></p>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t"
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__json_service__["a" /* JsonService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__json_service__["a" /* JsonService */]) === "function" && _a || Object])
], JsonComponent);

var _a;
//# sourceMappingURL=json.component.js.map

/***/ }),

/***/ 101:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavigationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var NavigationComponent = (function () {
    function NavigationComponent() {
    }
    return NavigationComponent;
}());
NavigationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'navigation',
        styles: ["\n\t\t.nav-link {\n\t\t\tcolor: #ffffff;\t\n\t\t}\n\t\t.active {\n\t\t\tcolor: #dd0000 !important;\n\t\t}\n\t"],
        template: "\n\t\t<ul class=\"nav nav-tabs nav-fill\">\n\t\t\t<li class=\"nav-item\">\n\t\t\t\t<a class=\"nav-link\" routerLink=\"varnish\" routerLinkActive=\"active\">Varnish</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item\">\n\t\t\t\t<a class=\"nav-link\" routerLink=\"rss\" routerLinkActive=\"active\">RSS</a>\n\t\t\t</li>\n\t\t\t<li class=\"nav-item\">\n\t\t\t\t<a class=\"nav-link\" routerLink=\"json\" routerLinkActive=\"active\">JSON</a>\n\t\t\t</li>\n\t\t</ul>\n\t"
    })
], NavigationComponent);

//# sourceMappingURL=navigation.component.js.map

/***/ }),

/***/ 102:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FeedComponent = (function () {
    function FeedComponent() {
        var _this = this;
        this.update = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* EventEmitter */]();
        // Update the feed every 60 seconds.
        this.timer = setInterval(function () {
            _this.updateFeed();
        }, 60000);
    }
    FeedComponent.prototype.getSortedItems = function () {
        return this.items.sort(function (n1, n2) {
            if (n1.getDate() < n2.getDate()) {
                return 1;
            }
            else if (n1.getDate() > n2.getDate()) {
                return -1;
            }
            else {
                return 0;
            }
        });
    };
    // Calls function in parent component.
    FeedComponent.prototype.updateFeed = function () {
        this.update.emit("");
    };
    return FeedComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", Array)
], FeedComponent.prototype, "items", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], FeedComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], FeedComponent.prototype, "description", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], FeedComponent.prototype, "url", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_3" /* Output */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* EventEmitter */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* EventEmitter */]) === "function" && _a || Object)
], FeedComponent.prototype, "update", void 0);
FeedComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'feed',
        styles: ["\n\t\t.btn-background {\n\t\t\tbackground-color: #dd0000;\n\t\t\tborder-color: #dd0000;\n\t\t}\n\n\t\t.cursor-pointer\u00A0{\n\t\t\tcursor: pointer;\n\t\t}\n\t"],
        template: "\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-12\">\n\t\t\t\t<div class=\"row\">\n\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t<!-- Incorrect use of CSS. -->\n\t\t\t\t\t\t<button class=\"btn btn-primary btn-background mt-5\" style=\"cursor: pointer;\" (click)=\"updateFeed()\">Oppdater</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"card my-3\" *ngFor=\"let item of getSortedItems()\">\n\t\t\t\t\t<item\n\t\t\t\t\t\t[date] = \"item.date\"\n\t\t\t\t\t\t[description] = \"item.description\"\n\t\t\t\t\t\t[link] = \"item.link\"\n\t\t\t\t\t\t[title] = \"item.title\"\n\t\t\t\t\t\t[url] = \"item.url\"\n\t\t\t\t\t\t[enclosures] = \"item.enclosures\"\n\t\t\t\t\t\t[imageRegularUrl] = \"item.imageRegularUrl\"\n\t\t\t\t\t></item>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t"
    }),
    __metadata("design:paramtypes", [])
], FeedComponent);

var _a;
//# sourceMappingURL=feed.component.js.map

/***/ }),

/***/ 103:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Enclosure; });
var Enclosure = (function () {
    function Enclosure(length, type, url) {
        this.length = length;
        this.type = type;
        this.url = url;
    }
    Enclosure.prototype.getLength = function () {
        return this.length;
    };
    Enclosure.prototype.getType = function () {
        return this.type;
    };
    Enclosure.prototype.getUrl = function () {
        return this.url;
    };
    return Enclosure;
}());

//# sourceMappingURL=enclosure.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ItemComponent = (function () {
    function ItemComponent() {
    }
    ItemComponent.prototype.getDate = function () {
        return this.date;
    };
    return ItemComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], ItemComponent.prototype, "date", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], ItemComponent.prototype, "description", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], ItemComponent.prototype, "link", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], ItemComponent.prototype, "title", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], ItemComponent.prototype, "url", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", Array)
], ItemComponent.prototype, "enclosures", void 0);
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* Input */])(),
    __metadata("design:type", String)
], ItemComponent.prototype, "imageRegularUrl", void 0);
ItemComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'item',
        styles: ["\n\t\t.stretch {\n\t\t\twidth: 100%;\n\t\t\theight: 100%;\n\t\t}\n\n\t\t.text-bold\u00A0{\n\t\t\tfont-weight: bold;\n\t\t}\n\t"],
        template: "\n\t\t<div class=\"card-block\">\n\t\t\t<div class=\"row\">\n\t\t\t\t<div class=\"col-12 col-md-3\">\n\t\t\t\t\t<div *ngFor=\"let enclosure of enclosures\">\n\t\t\t\t\t\t<img src=\"{{enclosure.getUrl()}}\" class=\"img-fluid d-block mx-auto stretch\" />\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t\t<div class=\"col-12 col-md-9\">\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t<p class=\"card-text text-right mb-2 text-bold\">{{date | date: 'HH:mm dd/MM-yyyy'}}</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t<h5 class=\"card-title\">{{title}}</h5>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t<p class=\"card-text\">{{this.description}}</p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t\t<div class=\"row\">\n\t\t\t\t\t\t<div class=\"col-12\">\n\t\t\t\t\t\t\t<p class=\"card-text\"><a href=\"{{url}}\">{{url}}</a></p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t\n\t\t\t\t\t<div class=\"row\" style=\"height: 100%;\">\n\t\t\t\t\t\t<div class=\"col-12\" style=\"height: 100%;\">\n\t\t\t\t\t\t\t<!-- Incorrect use of CSS -->\n\t\t\t\t\t\t\t<p class=\"card-text text-right\"><a href=\"{{link}}\" target=\"_blank\"><button class=\"btn btn-primary\" style=\"background-color: #dd0000; border: #dd0000; cursor: pointer;\">G\u00E5 til artikkel</button></a></p>\n\t\t\t\t\t\t</div>\n\t\t\t\t\t</div>\n\t\t</div>\n\t"
    }),
    __metadata("design:paramtypes", [])
], ItemComponent);

//# sourceMappingURL=item.component.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Item; });
var Item = (function () {
    function Item(date, description, link, title, url, enclosures, imageRegularUrl) {
        this.date = date;
        this.description = description;
        this.link = link;
        this.title = title;
        this.url = url;
        this.enclosures = enclosures;
        this.imageRegularUrl = imageRegularUrl;
    }
    Item.prototype.getDate = function () {
        return this.date;
    };
    Item.prototype.getDescription = function () {
        return this.description;
    };
    Item.prototype.getLink = function () {
        return this.link;
    };
    Item.prototype.getTitle = function () {
        return this.title;
    };
    Item.prototype.getUrl = function () {
        return this.url;
    };
    Item.prototype.getImageRegularUrl = function () {
        return this.imageRegularUrl;
    };
    return Item;
}());

//# sourceMappingURL=item.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__rss_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__feed_feed__ = __webpack_require__(61);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RSSComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RSSComponent = (function () {
    function RSSComponent(rssService) {
        this.rssFeed = '';
        this.rssFeedJson = new __WEBPACK_IMPORTED_MODULE_2__feed_feed__["a" /* Feed */]([], "", "", "");
        this.rssService = rssService;
    }
    RSSComponent.prototype.ngOnInit = function () {
        this.getRSSFeedJson();
    };
    RSSComponent.prototype.getRSSFeedJson = function () {
        var _this = this;
        this.rssService.getRSSFeedJson()
            .subscribe(function (feed) {
            _this.rssFeedJson = feed;
        });
    };
    return RSSComponent;
}());
RSSComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'rss',
        styles: ["\n\t\th2 {\n\t\t\tcolor: #ffffff;\n\t\t}\n\t"],
        template: "\n\t\t<h2 class=\"mt-5 text-center\">RSS</h2>\n\t\t<feed\n\t\t\t[items]=\"rssFeedJson.items\"\t\n\t\t\t[title]=\"rssFeedJson.title\"\n\t\t\t[description]=\"rssFeedJson.description\"\n\t\t\t[url]=\"rssFeedJson.url\"\n\t\t\t(update)=\"getRSSFeedJson()\"\n\t\t></feed>\n\t"
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__rss_service__["a" /* RSSService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__rss_service__["a" /* RSSService */]) === "function" && _a || Object])
], RSSComponent);

var _a;
//# sourceMappingURL=rss.component.js.map

/***/ }),

/***/ 107:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__varnish_service__ = __webpack_require__(63);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VarnishComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var VarnishComponent = (function () {
    function VarnishComponent(varnishService) {
        this.varnishService = varnishService;
        this.countedHosts = {};
        this.varnishLogRecords = [];
        this.maxHosts = [];
        this.maxHostValues = [];
    }
    VarnishComponent.prototype.ngOnInit = function () {
        this.getVarnishLogRecords();
    };
    VarnishComponent.prototype.getVarnishLogRecords = function () {
        var _this = this;
        this.varnishService.getVarnishLogRecords().subscribe(function (records) {
            _this.varnishLogRecords = records;
            _this.getTopHosts(_this.countHosts(_this.varnishLogRecords), 5);
            _this.getTopFiles(_this.countFiles(_this.varnishLogRecords), 5);
        });
    };
    VarnishComponent.prototype.countHosts = function (records) {
        var hostMap = {};
        for (var _i = 0, records_1 = records; _i < records_1.length; _i++) {
            var record = records_1[_i];
            hostMap[record.getHost()] = 0;
        }
        for (var _a = 0, records_2 = records; _a < records_2.length; _a++) {
            var record = records_2[_a];
            hostMap[record.getHost()] += 1;
        }
        return hostMap;
    };
    VarnishComponent.prototype.countFiles = function (records) {
        var fileMap = {};
        for (var _i = 0, records_3 = records; _i < records_3.length; _i++) {
            var record = records_3[_i];
            fileMap[record.getFile()] = 0;
        }
        for (var _a = 0, records_4 = records; _a < records_4.length; _a++) {
            var record = records_4[_a];
            fileMap[record.getFile()] += 1;
        }
        return fileMap;
    };
    VarnishComponent.prototype.getTopHosts = function (countedHosts, quantity) {
        var maxHost = this.getFirstObjectElement(countedHosts);
        var maxValue = countedHosts[maxHost];
        var maxHosts = [];
        var maxValues = [];
        for (var i = 0; i < quantity; i++) {
            for (var record in countedHosts) {
                if (countedHosts[record] >= countedHosts[maxHost]) {
                    maxHost = record;
                    maxValue = countedHosts[record];
                }
            }
            maxHosts.push(maxHost);
            maxValues.push(maxValue);
            countedHosts[maxHost] = -1;
        }
        this.maxHosts = maxHosts;
        this.maxHostValues = maxValues;
    };
    VarnishComponent.prototype.getTopFiles = function (countedFiles, quantity) {
        var maxFile = this.getFirstObjectElement(countedFiles);
        var maxValue = countedFiles[maxFile];
        var maxFiles = [];
        var maxValues = [];
        for (var i = 0; i < quantity; i++) {
            for (var record in countedFiles) {
                if (countedFiles[record] >= countedFiles[maxFile]) {
                    if (record.split('/').pop() != "") {
                        maxFile = record;
                        maxValue = countedFiles[maxFile];
                    }
                }
            }
            maxFiles.push(maxFile);
            maxValues.push(maxValue);
            countedFiles[maxFile] = -1;
        }
        this.maxFiles = maxFiles;
        this.maxFileValues = maxValues;
    };
    VarnishComponent.prototype.getFirstObjectElement = function (object) {
        for (var record in object) {
            return record;
        }
    };
    return VarnishComponent;
}());
VarnishComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'varnish',
        styles: ["\n\t\th2 {\n\t\t\tcolor: #ffffff;\n\t\t}\n\t\th4 {\n\t\t\tcolor: #dd0000;\n\t\t}\n\t\t.card {\n\t\t\tmax-width: 100%;\n\t\t}\n\t\t.card-text {\n\t\t\tword-wrap: break-word;\n\t\t}\n\t"],
        template: "\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-12\">\n\t\t\t\t<h2 class=\"mt-5 mb-0 text-center\">Varnish</h2>\n\t\t\t</div>\n\t\t</div>\n\t\t<div class=\"row\">\n\t\t\t<div class=\"col-lg-6 col-12\">\n\t\t\t\t<div class=\"card mt-5\">\n\t\t\t\t\t<div class=\"card-block\">\n\t\t\t\t\t\t<h4 class=\"card-title\">Top 5 Hosts</h4>\n\t\t\t\t\t\t<p class=\"card-text\" *ngFor=\"let record of maxHosts; let i = index;\">{{i+1}}. {{record}}, antall: {{maxHostValues[i]}}</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\n\t\t\t<div class=\"col-lg-6 col-12\">\n\t\t\t\t<div class=\"card my-5\">\n\t\t\t\t\t<div class=\"card-block\">\n\t\t\t\t\t\t<h4 class=\"card-title\">Top 5 Files</h4>\n\t\t\t\t\t\t<p class=\"card-text\" *ngFor=\"let record of maxFiles; let i = index;\">{{i+1}}. {{record}}, antall: {{maxFileValues[i]}}</p>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t</div>\n\t"
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__varnish_service__["a" /* VarnishService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__varnish_service__["a" /* VarnishService */]) === "function" && _a || Object])
], VarnishComponent);

var _a;
//# sourceMappingURL=varnish.component.js.map

/***/ }),

/***/ 108:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VarnishLogRecord; });
var VarnishLogRecord = (function () {
    function VarnishLogRecord(file, host) {
        this.file = file;
        this.host = host;
        if (this.host == null) {
            this.host = "";
        }
    }
    VarnishLogRecord.prototype.getFile = function () {
        return this.file;
    };
    VarnishLogRecord.prototype.getHost = function () {
        var hostname = "";
        if (this.host.indexOf("://") > -1) {
            hostname = this.host.split('/')[2];
        }
        else {
            hostname = this.host.split('/')[0];
        }
        hostname = hostname.split(':')[0];
        return hostname;
    };
    return VarnishLogRecord;
}());

//# sourceMappingURL=varnishLogRecord.js.map

/***/ }),

/***/ 109:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(28)();
// imports


// module
exports.push([module.i, ".container {\n\tbackground-color: #4d4d4d;\n}\n", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 166:
/***/ (function(module, exports) {

module.exports = "<navigation></navigation>\n<div class=\"container\">\n\t<div class=\"row\">\n\t\t<div class=\"col-12\">\n\t\t\t<router-outlet></router-outlet>\n\t\t</div>\n\t</div>\n</div>\n\n"

/***/ }),

/***/ 197:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(89);


/***/ }),

/***/ 60:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__json_article_json_article__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JsonService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var JsonService = (function () {
    function JsonService(http) {
        this.http = http;
    }
    JsonService.prototype.getJsonTestData = function () {
        /*
            TODO:
            Må ordne denne slik at den leser filen fra 'http://localhost:3000/files/testfeed.json'.

        */
        var _this = this;
        return this.http.get('http://localhost:3000/jsonTestfeed')
            .map(function (response) {
            var jsonArticles = [];
            var jsonResponse = response.json();
            for (var i = 0; i < jsonResponse.length; i++) {
                jsonArticles.push(new __WEBPACK_IMPORTED_MODULE_2__json_article_json_article__["a" /* JsonArticle */](jsonResponse[i].title, jsonResponse[i].link, jsonResponse[i].description, jsonResponse[i].date, jsonResponse[i].time, jsonResponse[i].category));
            }
            return _this.sortJsonTestData(jsonArticles);
        });
    };
    JsonService.prototype.sortJsonTestData = function (articles) {
        var _this = this;
        /*
            TODO:
            Vil gjerne fikse på denne slik at den sammenlikner Date objekter.
        */
        return articles.sort(function (n1, n2) {
            var n1DateTimeString = _this.convertDateToNumberFormat(n1.getDate(), n1.getTime());
            var n2DateTimeString = _this.convertDateToNumberFormat(n2.getDate(), n2.getTime());
            if (n1DateTimeString < n2DateTimeString) {
                return 1;
            }
            else if (n1DateTimeString > n2DateTimeString) {
                return -1;
            }
            else {
                return 0;
            }
        });
    };
    JsonService.prototype.convertDateToNumberFormat = function (dateString, time) {
        var date = dateString.split(' ')[0];
        var month = this.convertMonthToNumberFormat(dateString.split(' ')[1]);
        var year = dateString.split(' ')[2];
        return year + '-' + month + '-' + date + 'T' + time + ':00';
    };
    JsonService.prototype.convertMonthToNumberFormat = function (month) {
        month = month.toLowerCase();
        switch (month) {
            case 'januar':
                return '01';
            case 'februar':
                return '02';
            case 'mars':
                return '03';
            case 'april':
                return '04';
            case 'mai':
                return '05';
            case 'juni':
                return '06';
            case 'juli':
                return '07';
            case 'august':
                return '08';
            case 'september':
                return '09';
            case 'oktober':
                return '10';
            case 'november':
                return '11';
            case 'desember':
                return '12';
            default:
                return '01';
        }
    };
    return JsonService;
}());
JsonService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], JsonService);

var _a;
//# sourceMappingURL=json.service.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Feed; });
var Feed = (function () {
    function Feed(items, title, description, url) {
        this.items = items;
        this.title = title;
        this.description = description;
        this.url = url;
    }
    Feed.prototype.getItems = function () {
        return this.items;
    };
    Feed.prototype.getTitle = function () {
        return this.title;
    };
    Feed.prototype.getDescription = function () {
        return this.description;
    };
    Feed.prototype.getUrl = function () {
        return this.url;
    };
    return Feed;
}());

//# sourceMappingURL=feed.js.map

/***/ }),

/***/ 62:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__feed_feed__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__feed_item_item__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__feed_item_enclosure__ = __webpack_require__(103);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RSSService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RSSService = (function () {
    function RSSService(http) {
        this.http = http;
    }
    RSSService.prototype.getRSSFeedJson = function () {
        return this.http.get('http://localhost:3000/rssFeed')
            .map(function (response) {
            var feedJson = response.json();
            var items = [];
            for (var _i = 0, feedJson_1 = feedJson; _i < feedJson_1.length; _i++) {
                var item = feedJson_1[_i];
                var enclosures = [];
                for (var _a = 0, _b = item.enclosures; _a < _b.length; _a++) {
                    var enclosure = _b[_a];
                    enclosures.push(new __WEBPACK_IMPORTED_MODULE_5__feed_item_enclosure__["a" /* Enclosure */](enclosure.length, enclosure.type, enclosure.url));
                }
                items.push(new __WEBPACK_IMPORTED_MODULE_4__feed_item_item__["a" /* Item */](item.date, item.description, item.link, item.title, item.url, enclosures, item['rss:imgregular']['#']));
            }
            return new __WEBPACK_IMPORTED_MODULE_3__feed_feed__["a" /* Feed */](items, feedJson.title, feedJson.description, feedJson.url);
        });
    };
    return RSSService;
}());
RSSService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], RSSService);

var _a;
//# sourceMappingURL=rss.service.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__varnishLogRecord__ = __webpack_require__(108);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VarnishService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var VarnishService = (function () {
    function VarnishService(http) {
        this.http = http;
    }
    VarnishService.prototype.getVarnishLogRecords = function () {
        return this.http.get('http://localhost:3000/varnishLogFile')
            .map(function (response) {
            var records = [];
            var jsonRecords = response.json();
            for (var i = 0; i < jsonRecords.length; i++) {
                records.push(new __WEBPACK_IMPORTED_MODULE_3__varnishLogRecord__["a" /* VarnishLogRecord */](jsonRecords[i].file, jsonRecords[i].host));
            }
            return records;
        });
    };
    return VarnishService;
}());
VarnishService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["c" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]) === "function" && _a || Object])
], VarnishService);

var _a;
//# sourceMappingURL=varnish.service.js.map

/***/ }),

/***/ 88:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 88;


/***/ }),

/***/ 89:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(98);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(109);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 97:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = (function () {
    function AppComponent() {
    }
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_13" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(166),
        styles: [__webpack_require__(164)]
    })
], AppComponent);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 98:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_router__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__json_json_service__ = __webpack_require__(60);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__varnish_varnish_service__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__rss_rss_service__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__app_component__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__navigation_navigation_component__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__varnish_varnish_component__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__rss_rss_component__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__rss_feed_feed_component__ = __webpack_require__(102);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__rss_feed_item_item_component__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__json_json_component__ = __webpack_require__(100);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */],
            __WEBPACK_IMPORTED_MODULE_9__navigation_navigation_component__["a" /* NavigationComponent */],
            __WEBPACK_IMPORTED_MODULE_10__varnish_varnish_component__["a" /* VarnishComponent */],
            __WEBPACK_IMPORTED_MODULE_11__rss_rss_component__["a" /* RSSComponent */],
            __WEBPACK_IMPORTED_MODULE_12__rss_feed_feed_component__["a" /* FeedComponent */],
            __WEBPACK_IMPORTED_MODULE_13__rss_feed_item_item_component__["a" /* ItemComponent */],
            __WEBPACK_IMPORTED_MODULE_14__json_json_component__["a" /* JsonComponent */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__angular_router__["a" /* RouterModule */].forRoot([
                { path: '', redirectTo: '/varnish', pathMatch: 'full' },
                { path: 'varnish', component: __WEBPACK_IMPORTED_MODULE_10__varnish_varnish_component__["a" /* VarnishComponent */] },
                { path: 'rss', component: __WEBPACK_IMPORTED_MODULE_11__rss_rss_component__["a" /* RSSComponent */] },
                { path: 'json', component: __WEBPACK_IMPORTED_MODULE_14__json_json_component__["a" /* JsonComponent */] }
            ], { useHash: true })
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_5__json_json_service__["a" /* JsonService */],
            __WEBPACK_IMPORTED_MODULE_6__varnish_varnish_service__["a" /* VarnishService */],
            __WEBPACK_IMPORTED_MODULE_7__rss_rss_service__["a" /* RSSService */]
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_8__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 99:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return JsonArticle; });
var JsonArticle = (function () {
    function JsonArticle(title, link, description, date, time, category) {
        this.title = title;
        this.link = link;
        this.description = description;
        this.date = date;
        this.time = time;
        this.category = category;
    }
    JsonArticle.prototype.getTitle = function () {
        return this.title;
    };
    JsonArticle.prototype.getLink = function () {
        return this.link;
    };
    JsonArticle.prototype.getDescription = function () {
        return this.description;
    };
    JsonArticle.prototype.getDate = function () {
        return this.date;
    };
    JsonArticle.prototype.getTime = function () {
        return this.time;
    };
    JsonArticle.prototype.getCategory = function () {
        return this.category;
    };
    return JsonArticle;
}());

//# sourceMappingURL=json-article.js.map

/***/ })

},[197]);
//# sourceMappingURL=main.bundle.js.map