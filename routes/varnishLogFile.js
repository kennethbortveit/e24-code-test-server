module.exports = function(app, fs, request, urlParser, path){
	
	app.get('/varnishLogFile', function(req, res){
	
		var url = 'http://tech.vg.no/intervjuoppgave/varnish.log';
		
		request({
			url: url,
			json: false
		}, function(error, response, body){
			if(!error && response.statusCode === 200){
				var logData = body.split('\n');
				var jsonLogData = [];
				for(var i = 0; i < logData.length; i++){
					var logFields = logData[i].split(' ');
					if(logFields[6] != null && logFields[10] != null) {

						var host = logFields[10];
						var file = logFields[6];

						//Remove double quotes
						host = host.replace(/\"/g, '');
						file = file.replace(/\"/g, '');

						//Get remove query
						host = host.split('?')[0];
						file = file.split('?')[0];

						//Check for invalid data.
						if(file != "" && host != "-"){
							jsonLogData.push({
								file: file,
								host: host
							});
						}
					}
				}
				res.send(jsonLogData);	
			}
			else{
				console.log('Error: Could not get varnish.log file.');
			}
		});
	});
}
