module.exports = function(app, fs, require, Feed, rssParser){
	app.get('/rssFeed', function(req, res){
		var url = 'http://www.vg.no/rss/nyfront.php?frontId=1';
		rssParser(url, function(error, rss){
			if(error) {
				console.log('Could not get RSS feed.');
			} else {
				res.send(rss);
			}
		});
	});

};
